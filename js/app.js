import Form from "./components/form.js";
import Ticket from "./components/ticket.js";
import Header from "./components/header.js";
import Footer from "./components/footer.js";

export default {
  data() {
    return {
      formData: {},
      selectedValues: [],
      selectedValue: "No",
    };
  },
  components: {
    Header,
    Form,
    Ticket,
    Footer,
  },
  methods: {
    onInputChange(formData) {
      this.formData = formData;
    },

    handleCheckboxChange(values) {
      this.selectedValues = values;
    },
    handleSelectChange(value) {
      this.selectedValue = value;
    },
  },
  template: `
  <Header/>
  <div class="form-wrapper">
    <Form @inputChange="onInputChange" @checkbox-change="handleCheckboxChange" @select-changed="handleSelectChange"/>
    <Ticket :formData="formData" :selectedValues="selectedValues" :selectedValue="selectedValue"/>
  </div>
  <Footer/>
  `,
};
