export default {
  template: `
  <footer class="footer">
    <div class="container">
      <div class="footer__wrapper">
        <div class="footer__left">
          <div class="footer__top">
            <div class="footer__top-inner">
              <div class="footer__top-item">
                <h6 class="footer__top-title ">Quick-links</h6>
                <ul class="footer-list">
                  <li class="footer-list__item"><a href="#">Air Astana Secure</a></li>
                  <li class="footer-list__item"><a href="#">Contact us</a></li>
                  <li class="footer-list__item"><a href="#">Special Services</a></li>
                  <li class="footer-list__item"><a href="#">Onboard</a></li>
                </ul>
              </div>
              <div class="footer__top-item">
                <h6 class="footer__top-title ">Quick-links</h6>
                <ul class="footer-list">
                  <li class="footer-list__item"><a href="#">Air Astana Secure</a></li>
                  <li class="footer-list__item"><a href="#">Contact us</a></li>
                  <li class="footer-list__item"><a href="#">Special Services</a></li>
                  <li class="footer-list__item"><a href="#">Onboard</a></li>
                </ul>
              </div>
              <div class="footer__top-item">
                <h6 class="footer__top-title ">Quick-links</h6>
                <ul class="footer-list">
                  <li class="footer-list__item"><a href="#">Air Astana Secure</a></li>
                  <li class="footer-list__item"><a href="#">Contact us</a></li>
                  <li class="footer-list__item"><a href="#">Special Services</a></li>
                  <li class="footer-list__item"><a href="#">Onboard</a></li>
                </ul>
              </div>
              <div class="footer__top-item">
                <h6 class="footer__top-title ">Quick-links</h6>
                <ul class="footer-list">
                  <li class="footer-list__item"><a href="#">Air Astana Secure</a></li>
                  <li class="footer-list__item"><a href="#">Contact us</a></li>
                  <li class="footer-list__item"><a href="#">Special Services</a></li>
                  <li class="footer-list__item"><a href="#">Onboard</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="footer__right">
          <div class="footer__right-bottom">
            <ul class="footer-right-contacts__list">
              <li class="footer-right-contacts__item">
                <div class="footer__right-contacts">
                  <h3>7777</h3>
                  <p>Call centre</p>
                </div>
              </li>
              <li class="footer-right-contacts__item">
                <div class="footer__right-contacts">
                  <h3>8888</h3>
                  <p>Call centre for Business</p>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
    `,
};
