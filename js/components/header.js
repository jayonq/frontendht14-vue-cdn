export default {
  template: `
  <header class="header">
    <div class="header__top">
        <div class="container">
            <div class="header__top-inner">
                <nav class="menu">
                    <ul class="menu__list">
                        <li class="menu__item menu__item--active">
                            <a class="menu__link" href="#">For me</a>
                        </li>
                        <li class="menu__item">
                            <a class="menu__link" href="#">
                            For business</a>
                        </li>
                    </ul>
                </nav>
                <div class="menu__right">
                    <ul class="menu__right-list">
                        <li class="menu__right__item">
                            <select name="language" id="language">
                                <option value="en">Eng</option>
                                <option value="ru">Рус</option>
                                <option value="kz">Қаз</option>
                            </select>
                        </li>
                        <li class="menu__right__item">
                            <select name="region" id="region" style="max-width: 168px;">
                                <option value="trk" data-title="Türkiye" data-alias="kzt" selected="selected">
                                Türkiye
                                </option>
                                <option value="akt" data-title="United Arab Emirates" data-alias="akt">
                                United Arab Emirates
                                </option>
                            </select>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="header__bottom">
        <div class="container">
            <div class="header__bottom--menu">
                <div class="header__bottom--menu--left">
                    <a id="logo" href="index.html"></a>
                    <ul class="menu-categories">
                        <li class="menu-categories__item">
                            <a class="menu-categories__link" href="#"> Individual tours</a>
                        </li>
                        <li class="menu-categories__item">
                            <a class="menu-categories__link" href="#">group tours</a>
                        </li>
                    </ul>
                </div>
                <div class="header__bottom--menu--right">
                    <ul class="user-list">
                        <li class="user-list__item">
                            <a class="user-list__link search-icon" href="#"></a>
                        </li>
                        <li class="user-list__item">
                            <a class="user-list__link signup-icon" href="#"></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>    
    `,
};
