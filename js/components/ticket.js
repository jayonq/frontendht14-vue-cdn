export default {
  template: `
  <div class="section">
    <div class="ticket">
      <div class="ticket-header">
        <h2>Turkish Airlines</h2>
        <div class="triangle-left"></div>
        <div class="triangle-right"></div>
      </div>
      <div class="ticket-body">
        <div class="cities-info">
          <div>
            <h4>From</h4>
            <p>ALM</p>
          </div>
          <div><span><i class="fas fa-plane"></i></span></div>
          <div>
            <h4>To</h4>
            <p>TR</p>
          </div>
        </div>
        <div class="info">
          <div class="passenger">
            <h4>Passenger</h4>
            <p>{{ formData.name }} {{ formData.lastName }} {{ formData.middleName }}</p>
          </div>
				<div class="info-item-container">
					<div class="item">
						<h4>Gate</h4>
						<p>B3</p>
					</div>
					<div class="item">
						<h4>Seat</h4>
						<p>11E</p>
					</div>
					<div class="item">
						<h4>Boards</h4>
						<p>10:25 AM</p>
					</div>
					<div class="item">
						<h4>Arrives</h4>
						<p>1:05 PM</p>
					</div>
					<div class="item">
						<h4>Group</h4>
						<p>3</p>
					</div>
					<div class="item">
						<h4>Flight</h4>
						<p>2023</p>
					</div>
          <div class="item">
            <h4>Passengers</h4>
            <p>{{ formData.numberOfPassengers }}</p>
          </div>
          <div class="item">
            <h4>Luggage</h4>
            <p>{{ selectedValue }}</p>
          </div>
				</div>
        <div>
          <h4>Comments</h4>
          <p>{{ formData.comments }}</p>
        </div>
        <div>
          <h4>Selected excursions</h4>
          <p>{{ selectedValues }}</p>
        </div>
			</div>
			<div class="barcode-container">
				<span class="barcode">|||||||||||||||||||||</span>
			</div>
		</div>
		<div class="ticket-footer-container">
			<div class="ticket-footer-1"><span class="barcode">|||||||||||||||||||||</span></div>
			<div class="ticket-footer-2">
				<h5>Airplane Ticket</h5>
			</div>
		</div>
	</div>
</div>
  `,
  props: ["formData", "selectedValues", "selectedValue"],
};
