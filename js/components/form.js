export default {
  data() {
    return {
      name: "",
      lastName: "",
      middleName: "",
      numberOfPassengers: 1,
      comments: "",
      selectedValue: "No",
      selectedOptions: [],
    };
  },
  computed: {
    options() {
      return [
        { label: "Unusual Routes", value: "Unusual Routes" },
        { label: "On the boat", value: "On the boat" },
        { label: "Hippodrome Square", value: "Hippodrome Square" },
        { label: "Palaces", value: "Palaces" },
      ];
    },
  },
  watch: {
    selectedOptions() {
      this.$emit("checkbox-change", this.selectedOptions);
    },
  },

  methods: {
    onInput(event) {
      this.$emit("inputChange", {
        name: this.name,
        lastName: this.lastName,
        middleName: this.middleName,
        numberOfPassengers: this.numberOfPassengers,
        comments: this.comments,
      });
    },
    emitSelectedValue() {
      this.$emit("select-changed", this.selectedValue);
    },
  },
  template: `
  <section class="commercial-offer--form">
  <div class="container">
      <div class="commercial-form__inner">
          <h1>Application for a tour of Istanbul</h1>
          <div class="form--body">
              <form action="">
                    <div class="form">
                        <label for="name">Name <span style="color: red;">*</span></label><br>
                        <input type="text" id="name" required @input="onInput" v-model="name">
                    </div>
                    <div class="form">
                        <label for="lastName">LastName <span style="color: red;">*</span></label><br>
                        <input type="text" id="lastName" required @input="onInput" v-model="lastName">
                    </div>
                    <div class="form">
                        <label for="middleName">MiddleName <span style="color: red;">*</span></label><br>
                        <input type="text" id="middleName" required @input="onInput" v-model="middleName">
                    </div>
                    <div class="form">
                        <label for="number-of-passengers">Number of passengers <span style="color: red;">*</span></label><br>
                        <input type="number" id="number-of-passengers" required @input="onInput" v-model="numberOfPassengers" >
                    </div>
                    <div class="form">
                        <label>Your comments:</label><br>
                        <textarea name="comments" @input="onInput" v-model="comments"></textarea>
                    </div>
                    <div class="form-check">
                        <p>Select excursions</p>
                        <ul>
                                <li v-for="option in options" :key="option.value">
                                    <label >
                                        <input type="checkbox" :value="option.value" v-model="selectedOptions" class="styled-checkbox">
                                        {{ option.label }}
                                    </label>
                                </li>
                        </ul>
                    </div>
                    <div class="luggage">
                        <p>Do you have luggage?</p>
                        <select v-model="selectedValue" @change="emitSelectedValue">
                            <option value="Yes">Yes</option>
                            <option value="No" selected >No</option>
                        </select>
                    </div>
                    <div class="form-btn">
                        <button type="submit" id="submit">Send</button>
                    </div>
              </form>
          </div>
      </div>
  </div>
</section>
  `,
};
